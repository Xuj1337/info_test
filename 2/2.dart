import 'ImmutableList.dart';

main() {
  ImmutableList immutableList = ImmutableList.create([1, '5', true]);
  print('initial list: ');
  immutableList.printList();

  immutableList.putToIndex(1, '123');
  print('List element at index 2 is ${immutableList.getElementByIndex(2)}');
  print('Deleted element is ${immutableList.deleteElementByIndex(0)}');

  print('final list: ');
  immutableList.printList();
}
