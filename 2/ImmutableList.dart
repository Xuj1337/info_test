class ImmutableList {
  final List<dynamic> _list;
  ImmutableList.create(this._list);

  void putToIndex(int index, dynamic element) {
    if (0 > index || index >= _list.length) {
      print('Index out of range');
      return;
    }
    _list.insert(index, element);
  }

  dynamic getElementByIndex(int index) {
    if (0 > index || index >= _list.length) {
      print('Index out of range');
      return;
    }
    return _list.elementAt(index);
  }

  dynamic deleteElementByIndex(int index) {
    if (0 > index || index >= _list.length) {
      print('Index out of range');
      return;
    }
    return _list.removeAt(index);
  }

  void printList() {
    print(_list);
  }
}
