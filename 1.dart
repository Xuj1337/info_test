import 'dart:convert';
import 'dart:io';

main() {
  print('enter the natural number:');
  int? n;
  try {
    n = int.parse(stdin.readLineSync(encoding: Encoding.getByName('utf-8')!)!);
    if (n < 1) {
      print('enter correct value!');
      return;
    }
  } catch (e) {
    print('enter correct value!');
    return;
  }

  List<int> primitiveList = [];
  for (int i = 2; i <= n; i++) {
    bool isSimple = true;
    for (int j in primitiveList) {
      if (i % j == 0) {
        isSimple = false;
        break;
      }
    }
    if (isSimple) {
      primitiveList.add(i);
    }
  }

  print(primitiveList);
}
