import 'dart:convert';
import 'dart:io';

String? checkStringToBracketsSequence(String line) {
  List<String> openBrackets = ['(', '[', '{'];
  List<String> closeBrackets = [')', ']', '}'];
  List<String> compareList = [];

  if (!line.contains('(') &&
      !line.contains(')') &&
      !line.contains('[') &&
      !line.contains(']') &&
      !line.contains('{') &&
      !line.contains('}')) 
      {
        return ('Enter the string with brackets');
      }
      

  for (int i = 0; i < line.length; i++) {
    var char = line[i];
    if (openBrackets.contains(char)) {
      compareList.add(char);
    }
    ;
    if (closeBrackets.contains(char)) {
      if (compareList.length == 0) {
        return 'Wrong Brackets Sequence';
      }

      String currentOpenBracket = openBrackets[closeBrackets.indexOf(char)];
      if (compareList.last == currentOpenBracket) {
        compareList.remove(compareList.last);
      } else {
        return "Wrong Brackets Sequence";
      }
    }
  }
  return compareList.isNotEmpty
      ? 'Wrong Brackets Sequence'
      : 'Сorrect Brackets Sequence';
}

main(List<String> args) {
  String line =
      stdin.readLineSync(encoding: Encoding.getByName('utf-8')!) ?? '';
  if (checkStringToBracketsSequence(line) != null) {
    print(checkStringToBracketsSequence(line));
  }
}
